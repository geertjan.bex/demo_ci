# base image provided bij Continuum with miniconda3 preinstalled
FROM continuumio/miniconda3:4.9.2

# ensure that the .bashrc contains initialization for conda
RUN conda init bash

# copy conda environment definition and initialization script
COPY ./environment.yml /tmp/environment.yml
COPY ./init_environment.sh /tmp/init_environment.sh

# run the script to recreate the conda environment
WORKDIR /tmp
RUN bash init_environment.sh
