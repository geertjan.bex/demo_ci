#!/usr/bin/env bash

source ~/.bashrc
conda activate compute_env

mkdir -p output

jupyter nbconvert \
    --to=notebook \
    --ExecutePreprocessor.timeout=3600 \
    --execute \
    dashboard.ipynb
