#!/usr/bin/env bash

# ensure environment is setup properly for running conda
source ~/.bashrc

# create the environment
conda env create -f /tmp/environment.yml
