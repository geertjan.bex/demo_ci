#!/usr/bin/env bash

source ~/.bashrc
conda activate compute_env

mkdir -p data

python world_in_data.py --out data/world_data.csv
