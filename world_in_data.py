#!/usr/bin/env python

from argparse import ArgumentParser
import pandas as pd


DATA_URL = 'https://covid.ourworldindata.org/data/owid-covid-data.csv'
METADATA_COLUMNS = ['continent', 'location', 'date']
COLUMNS_OF_INTEREST = ['location', 'date', 'new_cases_smoothed_per_million']

def transform(data):
    data['new_cases_smoothed_per_100k'] = data.new_cases_smoothed_per_million/10.0
    data.drop('new_cases_smoothed_per_million', axis=1, inplace=True)
    data_per_location = data.pivot(columns=['location'], index='date', values=['new_cases_smoothed_per_100k'])
    for c in data_per_location.new_cases_smoothed_per_100k.columns:
        data_per_location['incidence_14_days', c] = data_per_location.new_cases_smoothed_per_100k[c].rolling(14).sum()
    return data_per_location.tail(21).stack()


if __name__ == '__main__':
    arg_parser = ArgumentParser(description='download and preprocess World in Data'
                                            'COVID-19 data')
    arg_parser.add_argument('--out', help='output file')
    options = arg_parser.parse_args()

    # download data, drop missing values
    data = pd.read_csv(DATA_URL)
    data.dropna(subset=METADATA_COLUMNS, inplace=True)
    
    # select datasets per continent
    datasets = dict()
    for continent in data.continent.unique():
        datasets[continent] = data.query(f'continent == "{continent}"')[COLUMNS_OF_INTEREST].copy()

    # transform datasets
    transformed_datasets = dict()
    for continent, dataset in datasets.items():
        transformed_datasets[continent] = transform(dataset)
        transformed_datasets[continent]['continent'] = continent
        transformed_datasets[continent].reset_index(inplace=True)

    # reassemble data into single dataframe and export
    all_data = pd.concat(transformed_datasets.values())
    all_data.to_csv(options.out, index=False)
